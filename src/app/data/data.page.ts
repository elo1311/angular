import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-data',
  templateUrl: './data.page.html',
  styleUrls: ['./data.page.scss'],
})
export class DataPage implements OnInit {

    public dataUrl;
    public dataJson;
    public forms;
    public name;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.dataUrl = 'https://pokeapi.co/api/v2/pokemon/empoleon/';
    this.dataJson = this.http.get(this.dataUrl);
    this.dataJson.subscribe(data => {
        this.forms = data['forms'];
        this.name = data['name'];
        console.log(this.forms);
        console.log(this.name);
    });
    console.log(this.forms);
    console.log(this.name);
  }

}
