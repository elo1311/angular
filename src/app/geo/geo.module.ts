import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { GeoPageRoutingModule } from './geo-routing.module';

import { GeoPage } from './geo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeoPageRoutingModule
  ],
  providers: [
    Geolocation,
  ],
  declarations: [GeoPage]
})
export class GeoPageModule {}
